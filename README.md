# Fdf

Ce projet consiste à créer graphiquement la representation schématique (en “fils de
fer” ou “wireframe” en anglais) d’un terrain en relief en reliant différents points (x, y,
z) par des segments.
Vous pouvez afficher vos terrains selon plusieurs options:
* Deux vues : Parallele et Isometrique
* Deux colorations : Blancs et Naturelle

Pour lancer le projet : ./fdf <map>

Exemple d'affichage:

![42-parallel](pictures/42_parallel.png)

map 42.fdf en vue parallele


![42-iso](pictures/42_iso.png)

map 42.fdf en vue isometrique


![42-random](pictures/map_random.png)

map random en vue isometrique avec les couleurs naturelles


![help](pictures/help.png)

Menu d'aide qui s'affiche en appuyant sur 'h'

Note finale : 125%
