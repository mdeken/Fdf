/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:45:54 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/09 13:45:01 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <mlx.h>
# include "libft.h"

# define KEYCODE_ESC		53
# define KEYCODE_PLUS		69
# define KEYCODE_MOINS		78
# define KEYCODE_V			9
# define KEYCODE_W			13
# define KEYCODE_S			1
# define KEYCODE_A			0
# define KEYCODE_D			2
# define KEYCODE_C			8
# define KEYCODE_R			15
# define KEYCODE_H			4
# define SCALE_DEFAULT		25
# define SCALE				15
# define TRANS_DEFAUT_PARA	10
# define TRANS_DEFAUT_ISO	450
# define TRANS				10
# define WIN_SIZEX			1000
# define WIN_SIZEY			800
# define CTE_PARA			0.5
# define CTE_ISO1			0.5
# define CTE_ISO2			0.5
# define BLUE				0xFF0000
# define GREEN				0x126900
# define BROWN				0x004069
# define DEEP_GREEN			0x024F10
# define DEEP_BLUE			0x4F0B02

enum			e_view{para, iso};
enum			e_color{white, natural};

typedef struct	s_point
{
	int			x;
	int			y;
	int			z;
}				t_point;

typedef struct	s_size
{
	int			x;
	int			y;
}				t_size;

typedef	struct	s_env
{
	void		*mlx;
	void		*win;
	void		*img;
	char		*data;
	int			bpp;
	int			sizeline;
	int			endian;
	t_point		scale;
	t_point		transpara;
	t_point		transiso;
	int			view;
	int			color;
	t_size		size;
	t_point		**map;
	t_point		**proj;
	int			help;
}				t_env;

void			ft_init_img(t_env *env);
void			ft_init_env(t_env *env, char *win_name);
t_point			**ft_getmap(char *map_path, t_size *size);
t_point			ft_parallel(t_point a, t_env env);
t_point			ft_iso(t_point a, t_env env);
void			ft_transform(t_env *env);
void			ft_initpoint(t_point *pt, int xp, int yp, int zp);
void			ft_error_exit(char *error);
void			ft_pixel_to_img(t_env *env, int x, int y, unsigned long color);
void			ft_draw_map(t_env *env);
void			ft_drawline(t_env *env, t_point a, t_point b);
void			ft_reset(t_env *env);
void			ft_change_scale(t_env *e, int keycode);
void			ft_change_trans(t_env *e, int i, int keycode);
int				ft_color(t_env *e, int z);

#endif
