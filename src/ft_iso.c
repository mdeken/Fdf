/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iso.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:31:39 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/01 13:31:41 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_point	ft_iso(t_point a, t_env env)
{
	t_point new_pt;

	a.x = a.x * env.scale.x;
	a.y = a.y * env.scale.y;
	a.z = a.z * env.scale.z * 2;
	new_pt.x = CTE_ISO1 * a.x - CTE_ISO2 * a.y;
	new_pt.y = -a.z + ((CTE_ISO1 * a.x) / 2) + ((CTE_ISO2 * a.y) / 2);
	new_pt.x = new_pt.x + env.transiso.x;
	new_pt.y = new_pt.y + env.transiso.y;
	new_pt.z = a.z;
	return (new_pt);
}
