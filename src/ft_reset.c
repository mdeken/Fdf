/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reset.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:42:28 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/01 13:42:30 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_reset(t_env *env)
{
	mlx_destroy_image(env->mlx, env->img);
	ft_init_img(env);
	ft_transform(env);
	ft_draw_map(env);
}
