/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_img.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:45:37 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/01 13:45:40 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_init_img(t_env *env)
{
	char	*data;
	int		bpp;
	int		sizeline;
	int		endian;

	env->img = mlx_new_image(env->mlx, WIN_SIZEX, WIN_SIZEY);
	if (env->img == NULL)
		ft_error_exit("can't create a new image");
	data = mlx_get_data_addr(env->img, &bpp, &sizeline, &endian);
	env->data = data;
	env->bpp = bpp;
	env->sizeline = sizeline;
	env->endian = endian;
}
