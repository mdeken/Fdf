/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:32:50 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/21 15:08:15 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <stdlib.h>

static void		mlx_init_all(t_env *env, char *win_name)
{
	env->mlx = mlx_init();
	if (env->mlx == NULL)
		ft_error_exit("can't initialize mlx");
	env->win = mlx_new_window(env->mlx, WIN_SIZEX, WIN_SIZEY, win_name);
	if (env->win == NULL)
		ft_error_exit("can't initialize window");
	ft_init_img(env);
}

static t_point	**ft_initproj(t_size size)
{
	int		i;
	t_point	**new_map;

	i = -1;
	if ((new_map = (t_point **)malloc(sizeof(t_point *) * size.y)) == NULL)
		ft_error_exit("malloc error");
	while (++i < size.y)
	{
		if ((new_map[i] = (t_point *)malloc(sizeof(t_point) * size.x)) == NULL)
			ft_error_exit("malloc error");
	}
	return (new_map);
}

static void		ft_init_map(char *map_path, t_env *env)
{
	env->map = ft_getmap(map_path, &(env->size));
	env->view = para;
	env->help = 0;
}

void			ft_init_env(t_env *env, char *win_name)
{
	ft_init_map(win_name, env);
	mlx_init_all(env, win_name);
	env->proj = ft_initproj(env->size);
	ft_initpoint(&(env->scale), 25, 25, 2);
	ft_initpoint(&(env->transpara), 10, 10, 0);
	ft_initpoint(&(env->transiso), 450, 50, 0);
	ft_transform(env);
}
