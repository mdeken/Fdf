/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:25:44 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/28 11:43:20 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <stdlib.h>

static void	exit_fdf(t_env *env)
{
	int	i;

	mlx_destroy_image(env->mlx, env->img);
	i = -1;
	while (++i < env->size.y)
	{
		free(env->proj[i]);
		free(env->map[i]);
	}
	free(env->proj);
	free(env->map);
	exit(0);
}

static int	key_hook(int keycode, t_env *e)
{
	if (keycode == KEYCODE_ESC)
		exit_fdf(e);
	else
	{
		if (keycode == KEYCODE_V)
			e->view = (e->view + 1) % 2;
		else if (keycode == KEYCODE_C)
			e->color = (e->color + 1) % 2;
		else if (keycode == KEYCODE_W || keycode == KEYCODE_S
				|| keycode == KEYCODE_A || keycode == KEYCODE_D)
			ft_change_trans(e, TRANS, keycode);
		else if (keycode == KEYCODE_R)
			ft_change_trans(e, 0, keycode);
		else if (keycode == KEYCODE_PLUS || keycode == KEYCODE_MOINS)
			ft_change_scale(e, keycode);
		else if (keycode == KEYCODE_H)
			e->help = (e->help == 0) ? 1 : 0;
		ft_reset(e);
	}
	return (keycode);
}

static int	expose_hook(t_env *e)
{
	ft_reset(e);
	return (1);
}

int			main(int argc, char **argv)
{
	t_env	env;

	if (argc == 1)
		ft_error_exit("Usage : ./fdf map");
	ft_init_env(&env, argv[1]);
	ft_draw_map(&env);
	mlx_hook(env.win, 2, 4, key_hook, &env);
	mlx_expose_hook(env.win, expose_hook, &env);
	mlx_loop(env.mlx);
	return (0);
}
