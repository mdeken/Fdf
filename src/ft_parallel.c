/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parallel.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:31:29 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/01 13:31:31 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_point	ft_parallel(t_point a, t_env env)
{
	t_point	new_pt;

	a.x = a.x * env.scale.x;
	a.y = a.y * env.scale.x;
	a.z = a.z * env.scale.z;
	if (env.scale.z > 0)
		a.z = a.z * env.scale.z;
	a.x += env.transpara.x;
	a.y += env.transpara.y;
	new_pt.x = a.x + CTE_PARA * a.z;
	new_pt.y = a.y + (CTE_PARA * (-a.z)) / 2;
	new_pt.z = a.z;
	return (new_pt);
}
