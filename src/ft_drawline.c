/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_drawline.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:33:51 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/01 13:33:53 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	draw_bresenham(t_env *env, t_point a, t_point b, int color)
{
	t_point d;
	int		error;

	ft_initpoint(&d, b.x - a.x, b.y - a.y, 0);
	error = 0;
	while (a.x <= b.x && a.x < WIN_SIZEX && a.y < WIN_SIZEY)
	{
		if (a.x >= 0 && a.y >= 0)
			ft_pixel_to_img(env, a.x, a.y, color);
		if (2 * error + d.y < d.x)
			error += d.y;
		else
		{
			a.y++;
			error = error + d.y - d.x;
		}
		a.x++;
	}
}

static void	draw_bresenham2(t_env *env, t_point a, t_point b, int color)
{
	t_point d;
	int		error;

	ft_initpoint(&d, b.x - a.x, b.y - a.y, 0);
	error = 0;
	while (a.y <= b.y && a.y < WIN_SIZEY && a.x < WIN_SIZEX)
	{
		if (a.x >= 0 && a.y >= 0)
			ft_pixel_to_img(env, a.x, a.y, color);
		if (2 * error + d.x < d.y)
			error += d.x;
		else
		{
			a.x++;
			error = error + d.x - d.y;
		}
		a.y++;
	}
}

static void	draw_bresenham3(t_env *env, t_point a, t_point b, int color)
{
	t_point d;
	int		error;

	ft_initpoint(&d, b.x - a.x, b.y - a.y, 0);
	d.y *= -1;
	error = 0;
	while (a.x <= b.x)
	{
		if (a.x >= 0 && a.y >= 0 && a.y < WIN_SIZEY && a.x < WIN_SIZEX)
			ft_pixel_to_img(env, a.x, a.y, color);
		if (2 * error + d.y < d.x)
			error += d.y;
		else
		{
			a.y--;
			error = error + d.y - d.x;
		}
		a.x++;
	}
}

static void	draw_bresenham4(t_env *env, t_point a, t_point b, int color)
{
	t_point d;
	int		error;

	ft_initpoint(&d, b.x - a.x, b.y - a.y, 0);
	d.y *= -1;
	error = 0;
	while (a.y >= b.y)
	{
		if (a.y < WIN_SIZEY && a.y >= 0 && a.x < WIN_SIZEX && a.x >= 0)
			ft_pixel_to_img(env, a.x, a.y, color);
		if (2 * error + d.x < d.y)
			error += d.x;
		else
		{
			a.x++;
			error = error + d.x - d.y;
		}
		a.y--;
	}
}

void		ft_drawline(t_env *env, t_point a, t_point b)
{
	int dx;
	int dy;
	int color;

	dx = b.x - a.x;
	dy = b.y - a.y;
	color = ft_color(env, b.z);
	if (dx < 0)
		ft_drawline(env, b, a);
	else
	{
		if (dy >= 0 && dx >= dy)
			draw_bresenham(env, a, b, color);
		else if (dy >= 0 && dx < dy)
			draw_bresenham2(env, a, b, color);
		else if (dy < 0 && dx >= -dy)
			draw_bresenham3(env, a, b, color);
		else if (dy < 0 && dx < -dy)
			draw_bresenham4(env, a, b, color);
	}
}
