/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_transform.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:32:16 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/01 13:32:19 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static t_point	new_point(t_point map_point, t_env env)
{
	if (env.view == para)
		return (ft_parallel(map_point, env));
	else if (env.view == iso)
		return (ft_iso(map_point, env));
	else
		return (ft_parallel(map_point, env));
}

void			ft_transform(t_env *env)
{
	int	i;
	int	j;

	i = -1;
	j = -1;
	while (++i < env->size.y)
	{
		j = -1;
		while (++j < env->size.x)
			env->proj[i][j] = new_point(env->map[i][j], *env);
	}
}
