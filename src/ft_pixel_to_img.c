/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pixel_to_img.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/09 18:20:27 by mdeken            #+#    #+#             */
/*   Updated: 2015/03/23 17:20:33 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_pixel_to_img(t_env *env, int x, int y, unsigned long color)
{
	unsigned char r;
	unsigned char g;
	unsigned char b;

	r = ((color & 0xFF0000) >> 16);
	g = ((color & 0xFF00) >> 8);
	b = (color & 0xFF);
	env->data[(y * env->sizeline) + (env->bpp * x / 8)] = r;
	env->data[(y * env->sizeline) + (env->bpp * x / 8) + 1] = g;
	env->data[(y * env->sizeline) + (env->bpp * x / 8) + 2] = b;
	env->data[(y * env->sizeline) + (env->bpp * x / 8) + 3] = 0;
}
