/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_change_scale.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:37:36 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/01 13:37:39 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_change_scale(t_env *e, int keycode)
{
	t_point	new_scale;

	new_scale = e->scale;
	if (new_scale.x >= 5)
	{
		if (keycode == KEYCODE_PLUS && new_scale.x < 2000)
		{
			new_scale.x += 5;
			new_scale.y += 5;
		}
		else if (new_scale.x > 5 && keycode == KEYCODE_MOINS)
		{
			new_scale.x -= 5;
			new_scale.y -= 5;
		}
	}
	ft_initpoint(&(e->scale), new_scale.x, new_scale.y, new_scale.z);
}
