/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_initpoint.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:29:46 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/01 13:29:48 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_initpoint(t_point *pt, int xp, int yp, int zp)
{
	pt->x = xp;
	pt->y = yp;
	pt->z = zp;
}
