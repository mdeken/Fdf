/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_change_trans.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:36:36 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/01 13:36:37 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_change_trans(t_env *e, int i, int keycode)
{
	int		new_trans;
	t_point	*new_pt;

	new_pt = (e->view == para) ? &(e->transpara) : &(e->transiso);
	if (keycode == KEYCODE_W)
		new_trans = new_pt->y - i;
	if (keycode == KEYCODE_S)
		new_trans = new_pt->y + i;
	if (keycode == KEYCODE_D)
		new_trans = new_pt->x + i;
	if (keycode == KEYCODE_A)
		new_trans = new_pt->x - i;
	if (keycode == KEYCODE_W || keycode == KEYCODE_S)
		ft_initpoint(new_pt, new_pt->x, new_trans, new_pt->z);
	else if (keycode == KEYCODE_D || keycode == KEYCODE_A)
		ft_initpoint(new_pt, new_trans, new_pt->y, new_pt->z);
	if (keycode == KEYCODE_R)
	{
		ft_initpoint(&(e->transpara), 10, 10, 0);
		ft_initpoint(&(e->transiso), 450, 50, 0);
	}
}
