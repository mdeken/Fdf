/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_map.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:35:03 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/07 15:36:23 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	print_menu(t_env *e)
{
	char	view[10];
	char	color[10];

	ft_strcpy(view, (e->view == iso) ? "Iso" : "Parallel");
	ft_strcpy(color, (e->color == white) ? "White" : "Natural");
	mlx_string_put(e->mlx, e->win, 5, WIN_SIZEY - 25, 0xFFFFFF, "View : ");
	mlx_string_put(e->mlx, e->win, 75, WIN_SIZEY - 25, 0xFFFFFF, view);
	mlx_string_put(e->mlx, e->win, 175, WIN_SIZEY - 25, 0xFFFFFF, "Color : ");
	mlx_string_put(e->mlx, e->win, 255, WIN_SIZEY - 25, 0xFFFFFF, color);
	if (e->help == 0)
	{
		mlx_string_put(e->mlx, e->win, 5, 5, 0xFFFFFF, "'h' : open help");
		return ;
	}
	mlx_string_put(e->mlx, e->win, 5, 5, 0xFFFFFF, "'h' : close help");
	mlx_string_put(e->mlx, e->win, 5, 25, 0xFFFFFF, "'w'/'a'/'s'/'d' : move");
	mlx_string_put(e->mlx, e->win, 5, 45, 0xFFFFFF, "'+'/'-' : zoom");
	mlx_string_put(e->mlx, e->win, 5, 65, 0xFFFFFF, "'c' : change color");
	mlx_string_put(e->mlx, e->win, 5, 85, 0xFFFFFF, "'v' : change view");
	mlx_string_put(e->mlx, e->win, 5, 105, 0xFFFFFF, "'r' : reset");
}

void	ft_draw_map(t_env *env)
{
	int	i;
	int	j;

	i = -1;
	while (++i < env->size.y)
	{
		j = -1;
		while (++j < env->size.x - 1)
			ft_drawline(env, env->proj[i][j], env->proj[i][j + 1]);
	}
	j = -1;
	while (++j < env->size.x)
	{
		i = -1;
		while (++i < env->size.y - 1)
			ft_drawline(env, env->proj[i][j], env->proj[i + 1][j]);
	}
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	print_menu(env);
}
