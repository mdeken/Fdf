/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:28:17 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/10 18:06:15 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

static int		size_y(char *map)
{
	int		fd;
	int		ret;
	char	*line;
	int		result;

	result = 0;
	if ((fd = open(map, O_RDONLY)) == -1)
		ft_error_exit("fdf : Can't open map");
	while ((ret = get_next_line(fd, &line)) > 0 && line != NULL)
	{
		free(line);
		result++;
	}
	close(fd);
	if (line == NULL || ret == -1)
		ft_error_exit("fdf : Error while reading file");
	return (result);
}

static int		size_x(char **digits)
{
	int	i;

	i = 0;
	while (digits[i] != '\0')
	{
		if (ft_isdigit(digits[i][0]) == 0 && digits[i][0] != '-')
			ft_error_exit("fdf : Map error please check your map");
		i++;
	}
	return (i);
}

static t_point	**fill_map(t_point **map, t_size *size, int fd, char **map_char)
{
	int		i;
	int		j;
	char	*line;

	i = 0;
	while ((get_next_line(fd, &line)))
	{
		j = 0;
		map_char = ft_strsplit(line, ' ');
		if (i == 0)
			size->x = size_x(map_char);
		else if (size->x != size_x(map_char))
			ft_error_exit("fdf : Map error, please check your map");
		if ((map[i] = (t_point *)malloc(sizeof(t_point) * size->x)) == NULL)
			ft_error_exit("fdf : Error malloc");
		while (map_char[j] != '\0')
		{
			ft_initpoint(&map[i][j], j, i, ft_atoi(map_char[j]));
			free(map_char[j++]);
		}
		free(line);
		free(map_char);
		i++;
	}
	return (map);
}

t_point			**ft_getmap(char *map_path, t_size *size)
{
	int		fd;
	t_point	**map;
	char	**map_char;

	if ((size->y = size_y(map_path)) == 0)
		ft_error_exit("fdf : Map empty");
	if ((map = (t_point **)malloc(sizeof(t_point *) * size->y)) == NULL)
		ft_error_exit("fdf : Error malloc");
	if ((fd = open(map_path, O_RDONLY)) == -1)
		ft_error_exit("fdf : Can't open map");
	map_char = NULL;
	map = fill_map(map, size, fd, map_char);
	if (size->x <= 1 && size->y <= 1)
		ft_error_exit("Not enought point");
	return (map);
}
