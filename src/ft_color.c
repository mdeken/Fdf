/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <mdeken@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 13:47:03 by mdeken            #+#    #+#             */
/*   Updated: 2015/12/01 13:47:06 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_color_natural(int z)
{
	if (z < 0 && z >= -100)
		return (BLUE);
	else if (z < -100)
		return (DEEP_BLUE);
	else if (z >= 0 && z < 100)
		return (GREEN);
	else if (z >= 100 && z < 250)
		return (DEEP_GREEN);
	else if (z >= 250 && z < 500)
		return (BROWN);
	else
		return (0xFFFFFF);
}

int			ft_color(t_env *e, int z)
{
	if (e->color == white)
		return (0xFFFFFF);
	if (e->color == natural)
		return (ft_color_natural(z));
	else
		return (0xFFFFFF);
}
